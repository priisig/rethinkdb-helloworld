package ch.tie.rethinkdbhelloworld;

public class TvShow {

  String id;
  String name;
  Long episodes;

  public TvShow() {

  }

  public TvShow(String id, String name, Long episodes) {
    this.name = name;
    this.episodes = episodes;
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getEpisodes() {
    return episodes;
  }

  public void setEpisodes(Long episodes) {
    this.episodes = episodes;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "[name: " + name + ", episodes: " + episodes + ", id: " + id + "]";
  }
}
