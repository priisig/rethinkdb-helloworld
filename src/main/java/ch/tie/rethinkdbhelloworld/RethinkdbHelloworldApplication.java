package ch.tie.rethinkdbhelloworld;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;

@SpringBootApplication
public class RethinkdbHelloworldApplication implements CommandLineRunner {

  public static final RethinkDB r = RethinkDB.r;

  public static void main(String[] args) {
    SpringApplication.run(RethinkdbHelloworldApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    Connection connection = RethinkDB.r.connection().hostname("localhost").connect();
    Cursor<TvShow> shows = r.db("test").table("tv_shows").run(connection, TvShow.class);
    for (TvShow tvShow : shows) {
      System.out.println(tvShow);
    }
  }
}
